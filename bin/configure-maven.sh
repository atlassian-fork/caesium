#!/bin/sh

#
# This file is managed by https://bitbucket.org/atlassian/armata-pipelines-primer/src/mvn-pac/.
#

# Add atlassian maven profiles, and maven servers, along with credentials for accessing them
# in the form of environment variables

sed --in-place='.bak' --file=- /usr/share/maven/conf/settings.xml<<END_SED
/<servers>/ r etc/servers.xml-fragment
/<profiles>/ r etc/profiles.xml-fragment
/<mirrors>/ r etc/mirrors.xml-fragment
END_SED
