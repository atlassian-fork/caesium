#!/bin/sh

#
# This file is managed by https://bitbucket.org/atlassian/armata-pipelines-primer/src/mvn-pac/.
#

set -ex

mvn --batch-mode --errors --no-snapshot-updates verify
