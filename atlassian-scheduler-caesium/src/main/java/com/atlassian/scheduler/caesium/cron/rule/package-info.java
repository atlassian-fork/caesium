/**
 * Tools for evaluating a parsed cron expression to find when it matches.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.caesium.cron.rule;

import javax.annotation.ParametersAreNonnullByDefault;