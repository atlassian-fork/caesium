package com.atlassian.scheduler.caesium.impl;

import com.atlassian.scheduler.caesium.spi.ClusteredJob;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.util.Safe;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Date;

import static java.util.Objects.requireNonNull;

/**
 * Simple, direct implementation of {@code ClusteredJob}.
 */
@Immutable
public class ImmutableClusteredJob implements ClusteredJob {
    private static final char[] HEX = {
            '0', '1', '2', '3',
            '4', '5', '6', '7',
            '8', '9', 'A', 'B',
            'C', 'D', 'E', 'F'
    };

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(ClusteredJob prototype) {
        return new Builder()
                .jobId(prototype.getJobId())
                .jobRunnerKey(prototype.getJobRunnerKey())
                .schedule(prototype.getSchedule())
                .nextRunTime(prototype.getNextRunTime())
                .version(prototype.getVersion())
                .parameters(prototype.getRawParameters());
    }

    private final JobId jobId;
    private final JobRunnerKey jobRunnerKey;
    private final Schedule schedule;
    private final Date nextRunTime;
    private final long version;
    private final byte[] rawParameters;

    ImmutableClusteredJob(Builder builder) {
        this.jobId = requireNonNull(builder.jobId, "jobId");
        this.jobRunnerKey = requireNonNull(builder.jobRunnerKey, "jobRunnerKey");
        this.schedule = requireNonNull(builder.schedule, "schedule");
        this.nextRunTime = Safe.copy(builder.nextRunTime);
        this.version = builder.version;
        this.rawParameters = Safe.copy(builder.rawParameters);
    }

    /**
     * Returns a new builder that is populated with the values from this immutable clustered job.
     * <p>
     * This convenience method is exactly equivalent to calling {@link #builder(ClusteredJob)}
     * with this object as the parameter.
     * </p>
     *
     * @return a new builder that is populated with the values from this immutable clustered job.
     */
    public Builder copy() {
        return builder(this);
    }

    @Override
    @Nonnull
    public JobId getJobId() {
        return jobId;
    }

    @Override
    @Nonnull
    public JobRunnerKey getJobRunnerKey() {
        return jobRunnerKey;
    }

    @Override
    @Nonnull
    public Schedule getSchedule() {
        return schedule;
    }

    @Override
    @Nullable
    public Date getNextRunTime() {
        return Safe.copy(nextRunTime);
    }

    @Override
    public long getVersion() {
        return version;
    }

    @Override
    @Nullable
    public byte[] getRawParameters() {
        return Safe.copy(rawParameters);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(256).append("ImmutableClusteredJob[jobId=").append(jobId)
                .append(",jobRunnerKey=").append(jobRunnerKey)
                .append(",schedule=").append(schedule)
                .append(",nextRunTime=").append(nextRunTime)
                .append(",version=").append(version)
                .append(",rawParameters=");
        if (rawParameters == null) {
            sb.append("(null)");
        } else {
            sb.append('[');
            appendBytes(sb, rawParameters);
            sb.append(']');
        }

        return sb.append(']').toString();
    }

    private static void appendBytes(final StringBuilder sb, final byte[] rawParameters) {
        final int stop = Math.min(rawParameters.length, 64);
        for (int i = 0; i < stop; ++i) {
            final int x = rawParameters[i];
            sb.append(HEX[(x & 0xF0) >> 4]);
            sb.append(HEX[x & 0x0F]);
        }

        if (stop < rawParameters.length) {
            sb.append("...");
        }
    }


    public static class Builder {
        JobId jobId;
        JobRunnerKey jobRunnerKey;
        Schedule schedule = Schedule.runOnce(null);
        Date nextRunTime;
        long version = 1L;
        byte[] rawParameters;

        public Builder jobId(JobId jobId) {
            this.jobId = requireNonNull(jobId, "jobId");
            return this;
        }

        public Builder jobRunnerKey(JobRunnerKey jobRunnerKey) {
            this.jobRunnerKey = requireNonNull(jobRunnerKey, "jobRunnerKey");
            return this;
        }

        public Builder schedule(Schedule schedule) {
            this.schedule = requireNonNull(schedule, "schedule");
            return this;
        }

        @SuppressWarnings("AssignmentToDateFieldFromParameter")  // Properly copied by build()
        public Builder nextRunTime(@Nullable Date nextRunTime) {
            this.nextRunTime = nextRunTime;
            return this;
        }

        public Builder version(long version) {
            this.version = version;
            return this;
        }

        @SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")  // Properly copied by build()
        public Builder parameters(@Nullable byte[] rawParameters) {
            this.rawParameters = rawParameters;
            return this;
        }

        public ImmutableClusteredJob build() {
            return new ImmutableClusteredJob(this);
        }
    }
}
