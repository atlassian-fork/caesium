package com.atlassian.scheduler.caesium.cron.rule.field;

import java.util.BitSet;

/**
 * A utility to deal with the annoying fact that cron uses 1=Sunday through 7=Saturday,
 * but we use the ISO values 1=Monday through 7=Sunday for our internal calculations.
 *
 * @since v0.0.1
 */
class DayOfWeekConstantConverter {
    private static final int CRON_TO_ISO[] = {-1, 7, 1, 2, 3, 4, 5, 6};
    private static final int ISO_TO_CRON[] = {-1, 2, 3, 4, 5, 6, 7, 1};
    private static final String ISO_TO_NAME[] = {null, "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};

    static int cronToIso(int cronDayOfWeek) {
        return CRON_TO_ISO[cronDayOfWeek];
    }

    static BitSet cronToIso(BitSet cronDaysOfWeek) {
        return mapBitSet(cronDaysOfWeek, CRON_TO_ISO);
    }

    static int isoToCron(int isoDayOfWeek) {
        return ISO_TO_CRON[isoDayOfWeek];
    }

    static BitSet isoToCron(BitSet isoDaysOfWeek) {
        return mapBitSet(isoDaysOfWeek, ISO_TO_CRON);
    }

    static String isoToName(int isoDayOfWeek) {
        return ISO_TO_NAME[isoDayOfWeek];
    }


    private static BitSet mapBitSet(BitSet inBits, int[] mapper) {
        final BitSet outBits = new BitSet(8);
        int inBit = inBits.nextSetBit(1);
        while (inBit != -1) {
            outBits.set(mapper[inBit]);
            inBit = inBits.nextSetBit(inBit + 1);
        }
        return outBits;
    }
}
