package com.atlassian.scheduler.caesium.cron.rule.field;

import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate.Field;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * A cron field implementation that is based on a range of accepted values.
 *
 * @since v0.0.1
 */
public class RangeFieldRule extends AbstractFieldRule {
    private static final long serialVersionUID = -8064232881796546833L;

    private final int min;
    private final int max;

    private RangeFieldRule(Field field, int min, int max) {
        super(field);
        this.min = min;
        this.max = max;
    }

    /**
     * Selects the most efficient implementation for the range provided.
     * <p>
     * If {@code min} and {@code max} are the same value, then the faster {@link SingleValueFieldRule}
     * implementation is substituted.
     * </p>
     *
     * @param field the field type for manipulating {@link DateTimeTemplate} values
     * @param min   the minimum value to include in the range; must be non-negative
     * @param max   the maximum value to include in the range; must be {@code >= min}
     * @return a field rule that implements the specified behaviour
     */
    public static FieldRule of(Field field, int min, int max) {
        checkArgument(min >= 0, "min >= 0");
        if (min == max) {
            return new SingleValueFieldRule(field, min);
        }

        checkArgument(min < max, "min < max");
        return new RangeFieldRule(field, min, max);
    }

    @Override
    public boolean matches(DateTimeTemplate dateTime) {
        final int value = get(dateTime);
        return value >= min && value <= max;
    }

    @Override
    public boolean first(DateTimeTemplate dateTime) {
        set(dateTime, min);
        return true;
    }

    @Override
    public boolean next(DateTimeTemplate dateTime) {
        final int value = get(dateTime);
        if (value >= max) {
            return false;
        }

        final int nextValue = (value < min) ? min : (value + 1);
        set(dateTime, nextValue);
        return true;
    }

    @Override
    protected void appendTo(StringBuilder sb) {
        if (min == field.getMinimumValue() && max == field.getMaximumValue()) {
            sb.append('*');
        } else {
            sb.append(min).append('-').append(max);
        }
    }
}
