package com.atlassian.scheduler.caesium.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Supplies the the threads that will be used to work the scheduler queue.
 *
 * @since v0.0.1
 */
class WorkerThreadFactory implements ThreadFactory {
    private static final Logger LOG = LoggerFactory.getLogger(WorkerThreadFactory.class);
    private static final AtomicInteger FACTORY_COUNTER = new AtomicInteger();
    private static final ClassLoader CLASS_LOADER = CaesiumSchedulerService.class.getClassLoader();

    private final String groupName = "Caesium-" + FACTORY_COUNTER.incrementAndGet();
    private final ThreadGroup group = new ThreadGroup(groupName);
    private final AtomicInteger threadCounter = new AtomicInteger();

    @Nonnull
    @Override
    public Thread newThread(Runnable runnable) {
        final String name = group.getName() + '-' + threadCounter.incrementAndGet();
        final Thread thd = new Thread(group, runnable, name);
        thd.setDaemon(true);
        thd.setContextClassLoader(CLASS_LOADER);
        LOG.debug("Creating new worker: {}", thd);
        return thd;
    }
}
