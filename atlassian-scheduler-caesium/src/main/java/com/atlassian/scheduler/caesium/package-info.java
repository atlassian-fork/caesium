/**
 * An independent, ground-up implementation of the atlassian-scheduler library.
 * <p>
 * Key components:
 * </p>
 * <ul>
 * <li>{@link com.atlassian.scheduler.caesium.impl.CaesiumSchedulerService CaesiumSchedulerService}
 * &mdash; the {@link com.atlassian.scheduler.SchedulerService SchedulerService} implementation</li>
 * <li>{@link com.atlassian.scheduler.caesium.cron.CaesiumCronExpressionValidator CaesiumCronExpressionValidator}
 * &mdash; the {@link com.atlassian.scheduler.cron.CronExpressionValidator CronExpressionValidator}
 * implementation</li>
 * <li>{@link com.atlassian.scheduler.caesium.spi.CaesiumSchedulerConfiguration CaesiumSchedulerConfiguration}
 * &mdash; the application-provided SPI for configuring the scheduler service</li>
 * <li>{@link com.atlassian.scheduler.caesium.spi.ClusteredJobDao ClusteredJobDao}
 * &mdash; the application-provided persistence layer for
 * {@link com.atlassian.scheduler.config.RunMode#RUN_ONCE_PER_CLUSTER RUN_ONCE_PER_CLUSTER} jobs</li>
 * </ul>
 */
package com.atlassian.scheduler.caesium;