package com.atlassian.scheduler.caesium.cron;

import com.atlassian.scheduler.core.tests.CronExpressionTimingTest;

/**
 * @since v0.0.1
 */
public class CaesiumCronExpressionTimingTest extends CronExpressionTimingTest {
    public CaesiumCronExpressionTimingTest() {
        super(new CaesiumCronFactory());
    }

    public static void main(String[] args) {
        new CaesiumCronExpressionTimingTest().test();
    }
}
