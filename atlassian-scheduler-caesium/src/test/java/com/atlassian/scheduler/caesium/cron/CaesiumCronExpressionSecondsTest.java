package com.atlassian.scheduler.caesium.cron;

import com.atlassian.scheduler.core.tests.CronExpressionSecondsTest;

/**
 * @since v0.0.1
 */
public class CaesiumCronExpressionSecondsTest extends CronExpressionSecondsTest {
    public CaesiumCronExpressionSecondsTest() {
        super(new CaesiumCronFactory());
    }
}
